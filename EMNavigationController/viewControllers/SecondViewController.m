//
//  SecondViewController.m
//  EMultiNavigation
//
//  Created by EasonWang on 13-11-10.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//

#import "SecondViewController.h"

#import "FirstViewController.h"
@interface SecondViewController ()

@end

@implementation SecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{

    [super viewWillDisappear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navTitle = @"Second";
    
    // 设置navigationBar右侧的item
    [self setRightItemWithTarget:self action:@selector(testRightItem) title:@"right"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)pushBut:(id)sender
{
    FirstViewController *first = [[FirstViewController alloc]initWithNibName:@"FirstViewController" bundle:nil];
    [NavigationController pushViewController:first animated:YES];
}

-(void)testRightItem
{
    NSLog(@"*********rightItem*********");
}

@end
